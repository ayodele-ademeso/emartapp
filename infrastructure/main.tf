terraform {
  required_version = ">= 1.0.0, < 2.0.0"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

data "aws_vpc" "main" {
  filter {
    name = "tag:Name"
    #values = ["${local.prefix}-vpc"]
    values = ["Default"]
  }
}

data "aws_subnets" "default" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.main.id]
  }
}

data "aws_subnet" "example" {
  for_each = toset(data.aws_subnets.default.ids)
  id       = each.value
}

data "aws_availability_zones" "all" {}

data "aws_iam_role" "task-execution" {
  name = "ecsTaskExecutionRole"
}

data "aws_iam_role" "ecs-service-role" {
  name = "AWSServiceRoleForECS"
}

data "aws_iam_policy" "ecs-policy" {
  arn = "arn:aws:iam::aws:policy/aws-service-role/AmazonECSServiceRolePolicy"
}

resource "aws_security_group" "loadbalancer" {
  name = var.lb_sg_name
  ingress {
    from_port   = var.port
    to_port     = var.port
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

#Create Security group for ECS service
resource "aws_security_group" "ecs-service" {
  name = var.ecs_sg_name
  ingress {
    from_port = var.container_port
    to_port   = var.container_port
    protocol  = "tcp"
    #cidr_blocks = ["0.0.0.0/0"]
    security_groups = [aws_security_group.loadbalancer.id]
  }

  egress {
    from_port        = 0
    to_port          = 0
    protocol         = "-1"
    cidr_blocks      = ["0.0.0.0/0"]
    ipv6_cidr_blocks = ["::/0"]
  }

  lifecycle {
    create_before_destroy = true
  }
}

#TO-DO: Create ECR

#Create Loadbalancer, listener, and Target group
resource "aws_lb_target_group" "emart-client" {
  name        = var.target_group_name
  port        = var.port
  protocol    = "HTTP"
  target_type = "ip"
  vpc_id      = data.aws_vpc.main.id
  health_check {
    path                = "/"
    port                = var.container_port
    healthy_threshold   = 2
    unhealthy_threshold = 2
    timeout             = 30
    interval            = 60
    matcher             = 200
  }
}

resource "aws_lb" "emartapp" {
  name               = var.lb_name
  internal           = false
  load_balancer_type = "application"
  security_groups    = [aws_security_group.loadbalancer.id]
  subnets            = [for subnet in data.aws_subnet.example : subnet.id] #pull public subnets from vpc

  #enable_deletion_protection = true

  #   access_logs {
  #     bucket  = aws_s3_bucket.lb_logs.bucket
  #     prefix  = "test-lb"
  #     enabled = true
  #   }

  tags = {
    Environment = "dev"
    Managed_By  = "Terraform"
  }
}

resource "aws_lb_listener" "emartapp" {
  load_balancer_arn = aws_lb.emartapp.arn
  port              = var.port
  protocol          = "HTTP"

  default_action {
    type             = "forward"
    target_group_arn = aws_lb_target_group.emart-client.arn
  }
}

resource "aws_ecs_cluster" "emart" {
  name = var.ecs_cluster_name

  setting {
    name  = "containerInsights"
    value = "enabled"
  }
  tags = {
    name       = "terraform-example"
    Managed-by = "Terraform"
  }
}

resource "aws_ecs_task_definition" "service" {
  family                   = "service"
  requires_compatibilities = ["FARGATE"]
  cpu                      = 1024
  memory                   = 2048
  network_mode             = "awsvpc"
  execution_role_arn       = data.aws_iam_role.task-execution.arn
  container_definitions = jsonencode([
    {
      name = "client"
      #TF_VAR_image_name = ${ECR_REGISTRY}:${CI_PIPELINE_IID} This is how the env variable will be set in the pipeline, then we can deploy a new image on every push to ECR
      image     = var.image_name
      cpu       = 512
      memory    = 1024
      essential = true
      portMappings = [
        {
          containerPort = var.container_port
          hostPort      = var.host_port
        }
      ]
    },
  ])
}

resource "aws_ecs_service" "client" {
  name                               = var.ecs-service-name
  cluster                            = aws_ecs_cluster.emart.id
  task_definition                    = aws_ecs_task_definition.service.arn
  launch_type                        = "FARGATE"
  desired_count                      = 2
  force_new_deployment               = true
  deployment_minimum_healthy_percent = 100
  deployment_maximum_percent         = 200
  #iam_role             = data.aws_iam_role.ecs-service-role.arn
  depends_on = [data.aws_iam_policy.ecs-policy]
  network_configuration {
    subnets          = data.aws_subnets.default.ids
    assign_public_ip = true
    security_groups  = [aws_security_group.ecs-service.id]
  }
  load_balancer {
    target_group_arn = aws_lb_target_group.emart-client.arn
    container_name   = "client"
    container_port   = var.container_port
  }
  lifecycle {
    create_before_destroy = true
  }
}
