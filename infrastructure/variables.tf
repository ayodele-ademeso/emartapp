# variable "security_group_id" {}

# variable "subnet_id" {}

variable "port" {
  description = "Port for security ecs-service sg"
  type        = number
  default     = 80
}

variable "container_port" {
  description = "Container port for task definition"
  type        = number
  default     = 4200
}

variable "host_port" {
  description = "Host port for task definition"
  type        = number
  default     = 4200
}

variable "image_name" {
  description = "Image name and tag"
  type        = string
}

variable "lb_name" {
  description = "The name of the ALB"
  type        = string
  default     = "terraform-asg-example"
}

variable "target_group_name" {
  description = "The name of the target group"
  type        = string
  default     = "tf-example-lb-tg"
}

variable "lb_sg_name" {
  description = "Name for the loadbalancer security group"
  type        = string
  default     = "ecs-lb-sg"
}

variable "ecs_sg_name" {
  description = "Name for the ecs service security group"
  type        = string
  default     = "ecs-service-sg"
}

variable "list_example" {
  description = "An example of a list in Terraform"
  type        = list(any)
  default     = [1, 2, 3]
}

variable "map_example" {
  description = "An example of a list in Terraform"
  type        = map(any)
  default = {
    key1 = "value1"
    key2 = "value2"
    key3 = "value3"
  }
}

variable "ecs_cluster_name" {
  description = "Name of ECS cluster"
  type        = string
  default     = "emart-app"
}

variable "ecs-service-name" {
  description = "Name of 'Client' ecs service"
  type        = string
  default     = "client-service"
}

variable "object_example" {
  description = "An example of a structural type in Terraform"
  type = object({
    name    = string
    age     = number
    tags    = list(string)
    enabled = bool
  })

  default = {
    name    = "value1"
    age     = 42
    tags    = ["a", "b", "c"]
    enabled = true
  }
}